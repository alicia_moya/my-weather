#python weather.py --city=Palma
import sys
import argparse
import requests

def parse_arguments():
    argparser = argparse.ArgumentParser('Return Weather data')
    argparser.add_argument('--city', help='Return the tomorrow forecast for the city', required=True)
    args = argparser.parse_args()
    return args

def get_city_forecast(city):
    url = 'http://localhost:5000/forecast/'+city
    req = None
    try:
        req = requests.get(url)
    except:
        print "The forecast service is not available"
    finally:
        if req is not None:
            set_answer(req)


def set_answer(response):
    if response.status_code == 404:
        print "The city does not exist"
    elif "city" in response.json():
        degrees = u'\u2103'
        res = "Weather forecast for tomorrow in %s\n    Temperature: %s%c"
        print res % (response.json()["city"], response.json()["temperature"], degrees)
    else:
        print 'The forecast service is not available'

def main():
    args = parse_arguments()
    try:
        get_city_forecast(args.city)
        sys.exit(0)
    except:
        sys.exit(1)

if __name__ == '__main__':
    main()
