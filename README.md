# Weather Tools:

Know the tomorrow forecast for any city!

## Installation

    pip install -r requirements.txt

## Usage

* First, launch the web service

    ```
    python forecast/main.py
    ```


* Then, ask for the city:

    ```
    python weather.py --city=<your-city>
    ```

## Tests

API functionality tests are included in the forecast directory. To run them:

    python forecast/forecast_tests.py