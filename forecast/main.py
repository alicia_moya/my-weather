from flask import Flask
import json

from forecast_consumer import Forecast

app = Flask(__name__)
forecast = Forecast()

@app.route('/forecast/<city>', methods=['GET'])
def get_forecast(city):
  tomorrow_forecast = forecast.get_tomorrow(city)
  if tomorrow_forecast == "not_found":
    return 'The city could not be found', 404
  else:
    return json.dumps({"city":city, "temperature":tomorrow_forecast})

if __name__ == '__main__':
  app.run()
