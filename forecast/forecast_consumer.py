import requests
import ConfigParser
import os
from datetime import datetime as dt

file_path = os.path.dirname(__file__)
conf_path = os.path.join(file_path, 'forecast.cfg')

config = ConfigParser.ConfigParser()
config.read(conf_path)

class Forecast(object):

  def __init__(self):
    self.api = config.get('API', 'url')
    self.apikey = config.get('API','key')
    self.units = config.get('API', 'units')
    ##check country code /weather?q={city},{country}

  def get_tomorrow(self,city):
    tomorrow = dt.today().day+1
    payload = {'q': city, 'APPID':self.apikey, 'units': self.units}
    req = requests.get(self.api, params=payload)
    if req.json()["cod"] == "404":
        return "not_found"
    else:
        tomorrow_list = [f_day["main"]["temp"] for f_day in req.json()["list"] if dt.utcfromtimestamp(f_day["dt"]).day is tomorrow]
        return sum(tomorrow_list)/len(tomorrow_list)
