import unittest
import forecast_consumer
import main
import json

class TestApi(unittest.TestCase):

  def test_has_city(self):
    now = json.loads(main.get_forecast("Sevilla"))
    self.assertTrue(now["city"])

  def test_has_temperature(self):
    now = json.loads(main.get_forecast("Sevilla"))
    self.assertTrue(now["temperature"])

  def test_does_not_have_city(self):
    now = main.get_forecast("reburgjitlsld")
    expected = ('The city could not be found', 404)
    self.assertEqual(now, expected)

  def test_temperature_is_float(self):
    now = json.loads(main.get_forecast("Madrid"))
    self.assertIsInstance(now["temperature"], type(1.0))

class TestForecast(unittest.TestCase):

  def setUp(self):
    self.forecast = forecast_consumer.Forecast()

  def test_tomorrow_has_data(self):
    self.assertTrue(self.forecast.get_tomorrow("Sevilla"))

  def test_no_city_return_not_found(self):
    self.assertEqual(self.forecast.get_tomorrow("reburgjitlsld"), "not_found")

if __name__ == '__main__':
  unittest.main()
